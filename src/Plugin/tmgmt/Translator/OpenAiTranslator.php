<?php

namespace Drupal\tmgmt_openai\Plugin\tmgmt\Translator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tmgmt\ContinuousTranslatorInterface;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\Translator\AvailableResult;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use OpenAI\Client;
use OpenAI\Responses\Chat\CreateResponse;
use Rajentrivedi\TokenizerX\TokenizerX;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * OpenAI translator plugin.
 *
 * @TranslatorPlugin(
 *   id = "openai",
 *   label = @Translation("OpenAI"),
 *   description = @Translation("OpenAI Translator service."),
 *   ui = "Drupal\tmgmt_openai\OpenAiTranslatorUi",
 *   logo = "icons/openai.svg",
 * )
 */
class OpenAiTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface, ContinuousTranslatorInterface {

  /**
   * OpenAI client.
   *
   */
  protected Client $client;

  /**
   * Data help service.
   *
   */
  protected Data $dataHelper;

  /**
   * Constructs a LocalActionBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\tmgmt\Data $data_helper
   *   Data helper service.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, Data $data_helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dataHelper = $data_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tmgmt.data')
    );
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::checkAvailable().
   */
  public function checkAvailable(TranslatorInterface $translator): AvailableResult {
    if ($translator->getSetting('openai_api_key')) {
      return AvailableResult::yes();
    }

    return AvailableResult::no($this->t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl()->toString(),
    ]));
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(JobInterface $job): void {
    $this->requestJobItemsTranslation($job->getItems());
    if (!$job->isRejected()) {
      $job->submitted('The translation job has been submitted.');
    }
  }

  /**
   * Build OpenAI client.
   */
  public static function openAiClient($translator): self {
    $instance = new self([], '', [], \Drupal::service('tmgmt.data'));
    try {
      if ($translator->getSetting('openai_api_service') == 'azure_openai') {
        $instance->client = \OpenAI::factory()
          ->withBaseUri($translator->getSetting('openai_api_base_url'))
          ->withHttpHeader('api-key', $translator->getSetting('openai_api_key'))
          ->withQueryParam('api-version', $translator->getSetting('openai_api_version'))
          ->make();
      }
      else {
        $org = !empty($translator->getSetting('openai_api_org')) ? $translator->getSetting('openai_api_org') : NULL;
        if (!empty($translator->getSetting('openai_api_key'))) {
          $instance->client = \OpenAI::client($translator->getSetting('openai_api_key'), $org);
        }
        else {
          \Drupal::messenger()->addMessage('Missing or incorrect OpenAI API Key', 'error');
        }
      }
    }
    catch (\Exception $ex) {
      \Drupal::messenger()->addMessage($ex->getMessage(), 'error');
    }
    return $instance;
  }

  /**
   * Split HTML into smaller chunks.
   *
   * @param array|string $q
   * @param mixed $maxChunkTokens
   * @return array $segments
   */
  public function htmlSplitter(array|string $q, mixed $maxChunkTokens): array {
    $doc = new \DOMDocument();
    @$doc->loadHTML(\mb_convert_encoding($q, 'HTML-ENTITIES', 'UTF-8'));

    $currentSegment = "";
    $currentTokens = 0;

    foreach ($doc->getElementsByTagName('body')->item(0)->childNodes as $node) {
      $nodeHTML = $doc->saveHTML($node);
      $tokens = $this->countTokens($nodeHTML);

      if ($currentTokens + $tokens > $maxChunkTokens) {
        $segments[] = $currentSegment;
        $currentSegment = "";
        $currentTokens = 0;
      }

      $currentTokens += $tokens;
      $currentSegment .= $nodeHTML;
    }

    if (!empty($currentSegment)) {
      $segments[] = $currentSegment;
    }
    return $segments;
  }

  /**
   * Split text into smaller chunks.
   *
   * @param array|string $q
   * @param mixed $maxChunkTokens
   * @return array $segments
   */
  public function textSplitter(array|string $q, mixed $maxChunkTokens): array {
    // Treat $q as plain text.
    $sentences = \explode(".", $q);
    $currentSegment = "";
    $currentTokens = 0;

    foreach ($sentences as $sentence) {
      $tokens = $this->countTokens($sentence);
      if ($currentTokens + $tokens > $maxChunkTokens) {
        $segments[] = \trim($currentSegment);
        $currentSegment = "";
        $currentTokens = 0;
      }
      // Prepend "." when adding a new sentence from 2nd round.
      if (!empty($currentSegment)) {
        $currentSegment .= ". ";
      }
      $currentSegment .= $sentence;
      $currentTokens += $tokens;
    }

    // For the remaining part of the text.
    if (!empty($currentSegment)) {
      $segments[] = \trim($currentSegment);
    }
    return $segments;
  }

  protected function getAllTextNodes($node): array {
    $textNodes = [];
    if ($node->nodeType == \XML_TEXT_NODE) {
      $textNodes[] = $node;
    }
    elseif ($node->nodeType == \XML_ELEMENT_NODE) {
      foreach ($node->childNodes as $child) {
        $textNodes = \array_merge($textNodes, $this->getAllTextNodes($child));
      }
    }
    return $textNodes;
  }

  protected function countTokens($text): int {
    return TokenizerX::count($text);
  }

  protected function isHtml($string): bool {
    return \preg_match("/<[^<]+>/", $string, $m) != 0;
  }

  /**
   * Creates a completion request for the provided prompt and parameters.
   *
   * @return \OpenAI\Responses\Chat\CreateResponse $response
   */
  public function completionsRequest($configuration): ?CreateResponse {
    try {
      $response = $this->client->chat()->create($configuration);
      return $response;
    }
    catch (\Exception $ex) {
      \Drupal::messenger()->addMessage($ex->getMessage(), 'error');
      return NULL;
    }
  }

  /**
   * Overrides
   * TMGMTDefaultTranslatorPluginController::getSupportedRemoteLanguages().
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator): array {
    $languages = [];
    $site_languages = \Drupal::languageManager()->getLanguages();
    foreach ($site_languages as $langcode => $language) {
      $languages[$langcode] = $language->getName();
    }
    return $languages;
  }

  /**
   * Overrides
   * TMGMTDefaultTranslatorPluginController::getSupportedTargetLanguages().
   */
  public function getSupportedTargetLanguages(TranslatorInterface $translator, $source_language): array {
    $languages = $this->getSupportedRemoteLanguages($translator);
    // There are no language pairs, any supported language can be translated
    // into the others. If the source language is part of the languages,
    // then return them all, just remove the source language.
    if (\array_key_exists($source_language, $languages)) {
      unset($languages[$source_language]);
      return $languages;
    }
    return [];
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::hasCheckoutSettings().
   */
  public function hasCheckoutSettings(JobInterface $job): bool {
    return FALSE;
  }

  /**
   * Local method to do request to OpenAI service.
   *
   * @param \Drupal\tmgmt\Entity\Translator $translator
   *   The translator entity to get the settings from.
   * @param string $action
   *   Action to be performed [translate, languages, detect]
   * @param array $request_query
   *   (Optional) Additional query params to be passed into the request.
   * @param array $options
   *   (Optional) Additional options that will be passed into
   *   drupal_http_request().
   *
   * @return string
   *   Translated string.
   *
   * @throws \Drupal\tmgmt\TMGMTException
   *   - Invalid action provided
   *   - Unable to connect to the Google Service
   *   - Error returned by the Google Service
   */
  protected static function doRequest(Translator $translator, $action, array $request_query = [], array $options = []): string {
    if (!\in_array($action, ['translate', 'languages'], TRUE)) {
      throw new TMGMTException('Invalid action requested: @action', ['@action' => $action]);
    }

    $chunk = $request_query['text'];
    $translatedChunk = '';

    try {
      $settings = $translator->getSettings();
      $site_languages = \Drupal::languageManager()->getLanguages();
      $client = self::openAiClient($translator);
      $prompt = $settings['openai']['prompt'] ?? 'Translate from %source% into %target% language';

      // Replace the source and target language in the prompt.
      $system_prompt = \str_replace(
        ['%source%', '%target%'],
        [
          $site_languages[$request_query['source']]->getName(),
          $site_languages[$request_query['target']]->getName(),
        ],
        $prompt
      );

      $configuration = [
        'messages' => [
          ['role' => 'system', 'content' => $system_prompt],
          ['role' => 'user', 'content' => $chunk],
        ],
        'model' => (string) $settings['openai']['model'],
        'temperature' => (float) $settings['openai']['temperature'],
        'max_tokens' => (int) $settings['openai']['max_tokens'],
        'top_p' => (float) $settings['openai']['top_p'],
        'frequency_penalty' => (float) $settings['openai']['frequency_penalty'],
        'presence_penalty' => (float) $settings['openai']['presence_penalty'],
      ];

      // Repeat the request until we get a response with maximum 5 attempts.
      for ($i = 0; $i < 5; $i++) {
        $response = $client->completionsRequest($configuration);
        if (!\is_null($response)) {
          break;
        }
      }

      if (!empty($response->choices[0])) {
        $translatedChunk = $response->choices[0]->message->content;
      }
    }
    catch (\Exception $e) {
      throw new TMGMTException('OpenAI service returned following error: @error', ['@error' => $e->getMessage()]);
    }

    // Throttling the request to maximum 5 calls per second.
    \usleep(1000_000);

    return $translatedChunk;
  }

  /**
   * {@inheritdoc}
   */
  public function requestJobItemsTranslation(array $job_items) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = \reset($job_items)->getJob();
    $settings = $job->getTranslator()->getSettings();
    $maxChunkTokens = $settings['openai']['max_chunk_tokens'] ?? 1024;

    foreach ($job_items as $job_item) {
      if ($job->isContinuous()) {
        $job_item->active();
      }
      // Pull the source data array through the job and flatten it.
      $data = $this->dataHelper->filterTranslatable($job_item->getData());

      $q = [];
      $keys_sequence = [];

      // Build OpenAI q param and preserve initial array keys.
      foreach ($data as $key => $value) {
        // Split the long text into chunks.
        $chunks = $this->isHtml($value['#text']) ? $this->htmlSplitter($value['#text'], $maxChunkTokens) : $this->textSplitter($value['#text'], $maxChunkTokens);
        $q[$key] = $chunks;
        $keys_sequence[] = $key;
      }

      $operations = [];
      $batch = [
        'title' => 'Translating job items',
        'finished' => [OpenAiTranslator::class, 'batchFinished'],
      ];

      foreach ($q as $key => $chunks) {
        foreach ($chunks as $_q) {
          if (\trim($_q) == "") {
            continue;
          }
          // Build operations array.
          $arg_array = [$job, $key, $_q, $keys_sequence];
          $operations[] = [
            '\Drupal\tmgmt_openai\Plugin\tmgmt\Translator\OpenAiTranslator::batchRequestTranslation',
            $arg_array,
          ];
        }
      }

      // Add beforeBatchFinished operation.
      $arg2_array = [$job_item];
      $operations[] = [
        '\Drupal\tmgmt_openai\Plugin\tmgmt\Translator\OpenAiTranslator::beforeBatchFinished',
        $arg2_array,
      ];
      // Set batch operations.
      $batch['operations'] = $operations;
    }
    \batch_set($batch);
  }

  /**
   * Batch 'operation' callback for requesting translation.
   *
   * @param \Drupal\tmgmt\Entity\Job $job
   *   The tmgmt job entity.
   * @param string $data_key
   *  The data key.
   * @param string $text
   *   The text to be translated.
   * @param array $keys_sequence
   *   Array of field name keys.
   * @param array $context
   *   The sandbox context.
   */
  public static function batchRequestTranslation(Job $job, string $data_key, string $text, array $keys_sequence, &$context): void {
    $translator = $job->getTranslator();

    // Build query params.
    $query_params = [
      'source' => $job->getSourceLangcode(),
      'target' => $job->getTargetLangcode(),
      'text' => $text,
    ];
    $result = self::doRequest($translator, 'translate', $query_params);

    if (!isset($context['results']['translation'])) {
      $context['results']['translation'] = [];
    }

    if (isset($context['results']['translation'][$data_key]) && $context['results']['translation'][$data_key]['#text'] != NULL) {
      $context['results']['translation'][$data_key]['#text'] .= "\n" . $result;
    }
    else {
      $context['results']['translation'][$data_key]['#text'] = $result;
    }
  }

  /**
   * Batch 'operation' callback.
   *
   * @param \Drupal\tmgmt\JobItemInterface $job_item
   *   The job item.
   * @param array $context
   *   The sandbox context.
   */
  public static function beforeBatchFinished(JobItemInterface $job_item, &$context): void {
    $context['results']['job_item'] = $job_item;
  }

  /**
   * Batch 'operation' callback.
   *
   * @param bool $success
   *   Batch success.
   * @param array $results
   *   Results.
   * @param array $operations
   *   Operations.
   */
  public static function batchFinished(bool $success, array $results, array $operations): void {
    $tmgmtData = \Drupal::service('tmgmt.data');

    if (isset($results['job_item']) && $results['job_item'] instanceof JobItemInterface) {
      $job_item = $results['job_item'];
      $job_item->addTranslatedData($tmgmtData->unflatten($results['translation']));
      $job = $job_item->getJob();
      \tmgmt_write_request_messages($job);
    }
  }

}
