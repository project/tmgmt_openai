<?php

namespace Drupal\tmgmt_openai;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;

/**
 * OpenAI translator UI.
 */
class OpenAiTranslatorUi extends TranslatorPluginUiBase {

  /**
   * OpenAI completion models.
   *
   * @var array $models
   */
  protected array $models = [
    "GPT-3.5" => [
      "gpt-3.5-turbo" => "gpt-3.5-turbo",
      "gpt-3.5-turbo-16k" => "gpt-3.5-turbo-16k",
    ],
    "GPT-4" => [
      "gpt-4" => "gpt-4",
      "gpt-4-32k" => "gpt-4-32k",
    ],
  ];

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    $settings = $translator->getSettings();
    $form['openai_api_service'] = [
      '#type' => 'select',
      '#title' => $this->t('Service Provider'),
      '#required' => TRUE,
      '#options' => [
        'openai' => $this->t('OpenAI'),
        'azure_openai' => $this->t('Microsoft Azure OpenAI'),
      ],
      '#default_value' => !empty($settings['openai_api_service']) ? $settings['openai_api_service'] : 'openai',
    ];
    $form['openai_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => !empty($settings['openai_api_key']) ? $settings['openai_api_key'] : '',
      '#description' => $this->t('Obtain your API Key from <a href="@link" target="_blank">here</a>', ['@link' => 'https://platform.openai.com/account/api-keys']),
      '#required' => TRUE,
    ];
    $form['openai_api_org'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Organization'),
      '#default_value' => !empty($settings['openai_api_org']) ? $settings['openai_api_org'] : '',
      '#states' => [
        'visible' => [
          ':input[name="settings[openai_api_service]"]' => ['value' => 'openai'],
        ],
      ],
    ];
    $form['openai_api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure OpenAI Base URL'),
      '#default_value' => !empty($settings['openai_api_base_url']) ? $settings['openai_api_base_url'] : '',
      '#description' => $this->t('Format example: {your-resource-name}.openai.azure.com/openai/deployments/{deployment-id}'),
      '#states' => [
        'visible' => [
          ':input[name="settings[openai_api_service]"]' => ['value' => 'azure_openai'],
        ],
      ],
    ];
    $form['openai_api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure OpenAI Version'),
      '#default_value' => !empty($settings['openai_api_version']) ? $settings['openai_api_version'] : '',
      '#states' => [
        'visible' => [
          ':input[name="settings[openai_api_service]"]' => ['value' => 'azure_openai'],
        ],
      ],
    ];
    $form['openai'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
    ];
    $form['openai']['prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prompt'),
      '#default_value' => !empty($settings['openai']['prompt']) ? $settings['openai']['prompt'] : "Translate from %source% into %target% language",
      '#description' => $this->t('Prompt of the system role. \n\nYou can adjust tone of the translated text and/or adjust style of the text. Supported tokens: <strong>%source%</strong> - source language (translate from), <strong>%target%</strong> - target language (translate to)'),
      '#placeholder' => "Translate from %source% into %target% language",
      '#required' => TRUE,
    ];
    $form['openai']['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#description' => $this->t('The model which will generate the completion. <a href="@link" target="_blank">Learn more</a>.', ['@link' => 'https://platform.openai.com/docs/models']),
      '#options' => $this->models,
      '#default_value' => !empty($settings['openai']['model']) ? $settings['openai']['model'] : 'gpt-3.5-turbo',
      '#required' => TRUE,
    ];
    $form['openai']['temperature'] = [
      '#type' => 'number',
      '#title' => $this->t('Temperature'),
      '#description' => $this->t('Controls randomness: Lowering results in less random completions. As the temperature approaches zero, the model will become deterministic and repetitive.'),
      '#min' => 0,
      '#max' => 2,
      '#step' => 0.01,
      '#default_value' => !empty($settings['openai']['temperature']) ? $settings['openai']['temperature'] : 0.7,
    ];
    $form['openai']['max_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum length'),
      '#description' => $this->t('The maximum number of tokens to generate. Requests can use up to 16,384 or 32,768 tokens shared between prompt and completion. The exact limit varies by model. (One token is roughly 4 characters for normal English text)'),
      '#min' => 1,
      '#max' => 32768 ,
      '#default_value' => !empty($settings['openai']['max_tokens']) ? $settings['openai']['max_tokens'] : 4096,
    ];
    $form['openai']['max_chunk_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum chunk tokens'),
      '#description' => $this->t('The maximum number of tokens to generate per chunk.'),
      '#min' => 1,
      '#max' => 32768 ,
      '#default_value' => !empty($settings['openai']['max_chunk_tokens']) ? $settings['openai']['max_chunk_tokens'] : 1024,
    ];
    $form['openai']['top_p'] = [
      '#type' => 'number',
      '#title' => $this->t('Top P'),
      '#description' => $this->t('Controls diversity via nucleus sampling: 0.5 means half of all likelihood-weighted options are considered.'),
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.01,
      '#default_value' => !empty($settings['openai']['top_p']) ? $settings['openai']['top_p'] : 1.0,
    ];
    $form['openai']['frequency_penalty'] = [
      '#type' => 'number',
      '#title' => $this->t('Frequency penalty'),
      '#description' => $this->t("How much to penalize new tokens based on their existing frequency in the text so far. Decreases the model's likelihood to repeat the same line verbatim."),
      '#min' => 0,
      '#max' => 2,
      '#step' => 0.01,
      '#default_value' => !empty($settings['openai']['frequency_penalty']) ? $settings['openai']['frequency_penalty'] : 0.0,
    ];
    $form['openai']['presence_penalty'] = [
      '#type' => 'number',
      '#title' => $this->t('Presence penalty'),
      '#description' => $this->t("How much to penalize new tokens based on whether they appear in the text so far. Increases the model's likelihood to talk about new topics."),
      '#min' => 0,
      '#max' => 2,
      '#step' => 0.01,
      '#default_value' => !empty($settings['openai']['presence_penalty']) ? $settings['openai']['presence_penalty'] : 1,
    ];
    $form += parent::addConnectButton();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    if (!empty($form_state->getValue('openai_api_key'))) {
      $connected = $translator->getPlugin()->openAiClient($translator)->getModels();
      if (empty($connected)) {
        $form_state->setErrorByName('settings][openai_api_key', $this->t('The "OpenAI API key" is not correct.'));
      }
    }
  }

}
