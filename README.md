# TMGMT Translator OpenAI

The TMGMT Translator OpenAI module is an OpenAI translator plugin
for the Translation Management Tools (TMGMT) project.
Allows to use translation available via OpenAI to translate content.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/tmgmt_openai).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/tmgmt_openai).

## Table of contents

- Requirements
- Installation
- Configuration
- Prompt Examples
- Maintainers

## Requirements

This module requires the following libraries (included in the composer.json):

- `openai-php/client`

The module also requires [Translation Management Tool](https://www.drupal.org/project/tmgmt)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Add OpenAI service provider for the translations (/admin/tmgmt/translators).

### [OpenAI](https://openai.com/)

1. Create API Key [here](https://platform.openai.com/account/api-keys)
2. Get API key and API Organization (optional)
3. You should be all set to use OpenAI integration

### Microsoft Azure OpenAI

For the Azure OpenAI you would need to specify the following in the plugin settings:

1. Base URL (Format: {your-resource-name}.openai.azure.com/openai/deployments/{deployment-id})
2. API Key
3. API Version

## Prompt Examples

1. Default prompt
```
Translate from %source% into %target% language

%text%
```

2. Ability to change style of the translation:

```
Translate from %source% into %target% language in Ernest Hemingway style

%text%
```

## Maintainers

- [Minnur Yunusov (minnur)](https://www.drupal.org/u/minnur)
